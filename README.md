# E5FIC-Virtualisation

## Group

Antoine BASLÉ - antoine.basle2@edu.esiee.fr

Quentin CABON - quentin.cabon@edu.esiee.fr


## Project

Java (Spring Boot) Web Service Calculator


Backend Service     => in a docker

Frontend Service    => in a docker.

Managed by Kubernetes.
