package com.example.backend;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Actions {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    
    private String actionName;
    private double number1;
    private double number2;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public double getNumber1() {
        return number1;
    }

    public void setNumber1(double number)
    {
        this.number1 = number;
    }

    public double getNumber2() {
        return number2;
    }

    public void setNumber2(double number)
    {
        this.number2 = number;
    }
}