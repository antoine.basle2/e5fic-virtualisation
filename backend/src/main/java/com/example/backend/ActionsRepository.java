package com.example.backend;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ActionsRepository extends CrudRepository<Actions, Integer> {

    List<Actions> findByName(String name);

}