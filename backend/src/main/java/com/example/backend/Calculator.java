package com.example.backend;

import java.lang.Math;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

@RestController
public class Calculator {

    @Autowired
    private ActionsRepository actionsRepository;
    
    @GetMapping("/add/{num1}/{num2}")
    public double add(@PathVariable double num1, @PathVariable double num2) {
        Actions a = new Actions();
        a.setActionName("add");
        a.setNumber1(num1);
        a.setNumber2(num2);
        actionsRepository.save(a);

        return num1+num2;
    }
    
    @GetMapping("/subtract/{num1}/{num2}")
    public double subtract(@PathVariable double num1, @PathVariable double num2) {
        Actions a = new Actions();
        a.setActionName("sub");
        a.setNumber1(num1);
        a.setNumber2(num2);
        actionsRepository.save(a);

        return num1-num2;
    }
    
    @GetMapping("/multiply/{num1}/{num2}")
    public double multiply(@PathVariable double num1, @PathVariable double num2) {
        Actions a = new Actions();
        a.setActionName("mul");
        a.setNumber1(num1);
        a.setNumber2(num2);
        actionsRepository.save(a);

        return num1*num2;
    }
    
    @GetMapping("/divide/{num1}/{num2}")
    public double divide(@PathVariable double num1, @PathVariable double num2) {
        Actions a = new Actions();
        a.setActionName("div");
        a.setNumber1(num1);
        a.setNumber2(num2);
        actionsRepository.save(a);

        return (num2==0) ? 0 : num1/num2;
    }
    
    @GetMapping("/power/{num1}/{num2}")
    public double power(@PathVariable double num1, @PathVariable double num2) {
        Actions a = new Actions();
        a.setActionName("pow");
        a.setNumber1(num1);
        a.setNumber2(num2);
        actionsRepository.save(a);

        return Math.pow(num1, num2);
    }
    
    @GetMapping("/sqrt/{num}")
    public double sqrt(@PathVariable double num) {
        Actions a = new Actions();
        a.setActionName("sqrt");
        a.setNumber1(num);
        actionsRepository.save(a);

        return Math.sqrt(num);
    }
}