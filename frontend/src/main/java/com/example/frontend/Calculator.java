package com.example.frontend;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

@RestController
public class Calculator {

    @Value("${backendURL}")
    String backEndURL;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String calculator(){
		try{
            RestTemplate restTemplate = new RestTemplate();
            String result = restTemplate.getForObject(backEndURL+"add/2/4", String.class);
            return "Result (front end) 2+4 = : " + result + " (back end)";
        }catch (Exception e){
            return e.getLocalizedMessage();
        }
    }

}